#include "stack.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


static char const* status_str(int status) {
  switch (status) {
  case EXIT_SUCCESS:   return "Nothing to broken";
  case NULL_STACK:     return "Pointer of stack is null";
  case STACK_EMPTY:    return "Stack contents are empty";
  case STACK_OVERFLOW: return "Stack overflow";
  case NOT_SET_DATA:   return "In cell of stack can't set data";
  case INCORRECT_DATA: return "Pointer of data can't be handle";
  default:             return "Unknown Status";
  }
}
void err_stack(int err) {
  printf("%s\n", status_str(err));
}


static int set_node(Node *node, void const *data, size_t size) {
  // General function
  //  for future, if will need change content struct Node
  node->data = malloc(size);
  if (node->data == NULL)
    return NOT_SET_DATA;

  memcpy(node->data, data, size);
  return EXIT_SUCCESS;
}
static void unset_node(Node *node) {
  free(node->data);
}
static void get_data(Node *node, void *data, size_t size) {
  // General function
  //  for future, if will need change content struct Node
  memcpy(data, node->data, size);
}

#ifdef STACK_ARRAY_H

bool empty(Stack const *stack) {
  if (stack == NULL)
    return false;

  return stack->top == 0;
}
static Node* pop_back(Stack *stack) {
  // Чтобы не забывать написать 
  //       stack->top - 1
  if (empty(stack)) {
    return NULL;
  }

  --stack->top;
  return stack->array + stack->top;
}
Stack create_stack(size_t sz_data, size_t length_array) {
  if (sz_data == 0) {
    exit(EXIT_FAILURE);
  }
  Stack stack = (Stack) {
                 .top   = 0,
                 .sz_data = sz_data,
                 .size  = length_array,
                 .array = (Node*) calloc(length_array, sizeof(Node))
                };

  if (stack.array == NULL) {
    exit(EXIT_FAILURE);
  }
  return stack;
}
void delete_stack(Stack *stack) {
  if (stack == NULL) {
    err_stack(NULL_STACK);
    return;
  }
 
  Node *top_node = pop_back(stack);
  while (top_node != NULL) { // пока не закончиться инфа
    unset_node(top_node);
    top_node = pop_back(stack);
  }

  free(stack->array);
}
int push(Stack *stack, void const *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  if (data == NULL) {
    return INCORRECT_DATA;
  }
  if (stack->top >= stack->size) {
    return STACK_OVERFLOW;
  }

  if (set_node(stack->array + stack->top, data, stack->sz_data) != EXIT_SUCCESS) {
    return NOT_SET_DATA;
  }

  ++stack->top;

  return EXIT_SUCCESS;
}
int pop(Stack *stack, void *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  if (data == NULL) {
    return INCORRECT_DATA;
  }

  Node *top_node = pop_back(stack);
  if (top_node == NULL) {
    return STACK_EMPTY;
  }
  get_data(top_node, data, stack->sz_data);
  unset_node(top_node);

  return EXIT_SUCCESS;
}

#elif STACK_LIST_H
       
bool empty(Stack const *stack) {
  if (stack == NULL)
    return false;

  return stack->top == NULL;
}
static Node* pop_back(Stack *stack) {
  if (empty(stack)) {
    return NULL;
  }
  Node *last_node = stack->top;
  stack->top = stack->top->next;

  last_node->next = NULL; // чтобы не было связи с другим
  return last_node;
}
Stack create_stack(size_t sz_data) {
  return (Stack) {
                  .sz_data = sz_data,
                  .top     = NULL
                 };
}
void delete_stack(Stack *stack) {
  if (stack == NULL) {
    err_stack(NULL_STACK);
    return;
  }

  Node *top_node = pop_back(stack);
  while (top_node != NULL) { // пока не закончиться инфа
    unset_node(top_node);
    free(top_node);
    top_node = pop_back(stack);
  }
}
int push(Stack *stack, void const *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  if (data == NULL) {
    return INCORRECT_DATA;
  }
  
  Node *new_node = (Node*) malloc(sizeof(Node));
  if (new_node == NULL) {
    return NOT_SET_DATA;
  }
  if (set_node(new_node, data, stack->sz_data) != 0) {
    free(new_node);
    return NOT_SET_DATA;
  }

  new_node->next = stack->top;
  stack->top = new_node;
  return EXIT_SUCCESS;
}
int pop(Stack *stack, void *data) {
  if (stack == NULL) {
    return NULL_STACK;
  }
  if (data == NULL) {
    return INCORRECT_DATA;
  }

  Node *top_node = pop_back(stack);
  get_data(top_node, data, stack->sz_data);
  unset_node(top_node);

  free(top_node);

  return EXIT_SUCCESS;
}

#endif // STACK_LIST_H
