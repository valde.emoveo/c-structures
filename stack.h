#ifndef STACK_H
#define STACK_H

#include <stdbool.h>
#include <stddef.h>

enum STATUS_FUNC {
  NULL_STACK = 10,
  STACK_EMPTY,
  STACK_OVERFLOW,
  NOT_SET_DATA,
  INCORRECT_DATA
};

typedef struct Node Node;
typedef struct Stack Stack;

#ifdef STACK_ARRAY_H

struct Node {
  void *data;
};
struct Stack {
  size_t       top;
  size_t const sz_data;
  size_t const size;
  Node        *array;
};

Stack create_stack(size_t sz_data, size_t length_array);

#elif STACK_LIST_H

struct Node {
  void *data;
  Node *next;
};
struct Stack {
  size_t const sz_data;
  Node *top;
};

Stack create_stack(size_t sz_data);

#endif // STACK_LIST_H

/* Common */
void delete_stack(Stack *stack);
bool        empty(Stack const *stack);
int          push(Stack *stack, void const *data);
int           pop(Stack *stack, void *data);
/* Common */

#endif // STACK_H
