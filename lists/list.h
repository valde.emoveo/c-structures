#ifndef LIST_LIST_H
#define LIST_LIST_H

void handle_error_list(int status);

struct List_t;

struct List_t create_list();
int           delete_list(struct List_t *list);

int push_front(struct List_t *list, void const *data);
int  pop_front(struct List_t *list, void *from_list);

#endif // LIST_LIST_H
