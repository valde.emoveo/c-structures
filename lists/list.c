#include "list.h"
#include <stdlib.h>
#include <stdio.h>


typedef ElemList_t ElemList;
struct ElemList_t {
  void              *data;
  struct ElemList_t *next;
};

struct List_t {
  ElemList *head;
};


static ElemList* create_elemlist(void const *data) {
  ElemList *element = malloc(sizeof(ElemList));
  if (element) {
    element->data = data;
    element->next = NULL;
  }
  return element;
}
static void delete_elemlist(ElemList *element) {
  free(element);
}


enum {
  NULL_ALLOCATE = 1,
  LIST_IS_NULL,
  NULL_ARG,
  NOTHING
};
static char const* msg_status(int status) {
  switch (status) {
    case NULL_ALLOCATE: return "Doesn't allocate memory in heap";
    case LIST_IS_NULL:  return "Ptr at list is null";
    case NULL_ARG:      return "Null args";
    case NOTHING:       return "Nothing in list";
    default:            return "Unknown status";
  }
}
void handle_error_list(int status) {
  printf("%s\n", msg_status(status));
}
struct List_t create_list() {
  return (struct List_t) {.head = NULL};
}
int delete_list(struct List_t *list) {
  if (!list)
    return LIST_IS_NULL;

  while (list->head) {
    ElemList *ptr = list->head; 
    list = list->head;
    delete_elemlist(ptr):
  }

  return EXIT_SUCCESS;
}
int push_front(struct List_t *list, void const *data) {
  if (!list) 
    return LIST_IS_NULL;
  if (!data) 
    return NULL_ARG; 

  ElemList *new_elem = create_elemlist(data);
  if (!new_elem)
    return NULL_ALLOCATE;

  new_elem->next = list->head;
  list->head = new_elem;
  
  return EXIT_SUCCESS; 
}
int pop_front(struct List_t *list, void *from_list) {
  if (!list)
    return LIST_IS_NULL;

  if (!from_list) 
    return NULL_ARG; 

  if (!list->head)
    return NOTHING;

  from_list = list->head;
  list->head = list->head->next;
  
  return EXIT_SUCCESS; 
}
